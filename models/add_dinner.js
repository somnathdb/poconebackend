//  Add Dinner Information Model 
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const dinnerSchema = new Schema({
    name: { type: String, require: true },
    phone_number: { type: Number, require: true },
    adults: { type: Number, require: true },
    kids: { type: Number, require: true },
    special_occassion: { type: String, require: true },
    total:{type:Number,require:true} ,
    vendor_id:{type:String,require:true},
    wait_time:{type:Number,require:true},
    status: { type: String, require: true }

});


module.exports = mongoose.model('Dinner', dinnerSchema);