const express = require('express');
const router = express.Router();
const Dinner = require('../models/add_dinner');
const default_time = require('../models/default');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys')
const add_dinner_validation = require('../validation/add_dinner');
const msg91 = require("msg91")("284439A1ytuiL455d245ad9", "SomiDb", "1");
//Sending SMS
const SMS = require('../validation/sms_sending');


// Get To All the Dinner List Using /all_dinner route

router.get('/all_dinner', (req, res, next) => {
    Dinner.find()
        .exec()
        .then(DinnerList => {
            res.status(200).json(DinnerList)
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
});

// Insert Dinner Using /add_dinner POST method

router.post('/add_dinner', (req, res, next) => {
    const { errors, isValid } = add_dinner_validation(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization;
        const token = authorization.split(" ")[1];
        try {
            decoded = jwt.verify(token, keys.secretOrKey);
        } catch (e) {
            return res.status(401).send('unauthorized');
        }
        const vendorId = decoded.id;
        const dinner = new Dinner({
            vendor_id: vendorId,
            name: req.body.name,
            phone_number: req.body.phone_number,
            adults: req.body.adults,
            kids: req.body.kids,
            total: parseInt(req.body.adults) + parseInt(req.body.kids),
            wait_time: req.body.wait_time,
            special_occassion: req.body.special_occassion,
            status: "Nonseated"
        });
        dinner
            .save()
            .then(result => {
                msg91.send(dinner.phone_number, "Your Order Is Confirmed..!", (err, response) => {
                    res.status(201).json({
                        message: 'Dinner Information Added Successfully...!',
                    });
                });
            })
            .catch(err => {
                res.status(500).json({ error: err });
            });
    }
});

//Update Satus 
router.patch('/status/:dinnerId', (req, res, next) => {
    const body = req.body;
    Dinner.findByIdAndUpdate({ _id: body.id }, { $set: { status: req.body.status } })
        .exec()
        .then(result => {
            res.status(200).json({ message: 'Status Update SuccessFully..!' });
        })
        .catch(err => {
            res.status(500).json({ message: 'Status Not Update SuccessFully..!' });
        });

});

//send sms

router.post('/send', (req, res) => {
    const { errors, isValid } = SMS(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    msg91.send(req.body.phone_number, req.body.message, (err, response) => {
        res.json(response);
    });
});


//Update Default Wait Time 
// router.patch('/wait_time', (req, res, next) => {
//     const body = req.body;
//     const default_wait_time = 0;
//     Dinner.findOne({ _id: body.id })
//         .exec()
//         .then(DinnerList => {
//             if (body.wait_time == null) {
//                 Dinner.findByIdAndUpdate({ _id: body.id }, { $set: { wait_time: default_wait_time } })
//                     .exec()
//                     .then(result => {
//                         res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
//                     })
//                     .catch(err => {
//                         res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
//                     });
//             }
//             else {
//                 Dinner.findByIdAndUpdate({ _id: body.id }, { $set: { wait_time: req.body.wait_time } })
//                     .exec()
//                     .then(result => {
//                         res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
//                     })
//                     .catch(err => {
//                         res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
//                     });

//             }
//         });
// });

//  Geting Waiting Time

router.get('/get_wait_time', (req, res, next) => {
    default_time.find()
        .exec()
        .then(default_time => {
            res.status(200).json(default_time)
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
});



// Update Wait_time with SMS (Gourav Send)
// router.patch("/update_waitTime", async (req, res, next) => {
//     try {
//         const body = req.body;
//         var updateDinner = await Dinner.findOneAndUpdate({ _id: body._id }, {
//             $set: { wait_time: body.wait_time }
//         })
//         if (updateDinner) {
//             return res.status(200).json("Updated Successfully!!!");
//         }
//         Dinner.findOne({ _id: body.id })
//             .exec()
//             .then(async DinnerList => {
//                 msg91.send(DinnerList.phone_number, ('Your Waiting Time is ' + body.wait_time + ' Minutes'), (err, response) => {
//                 });
//             });
//     } catch (err) {
//         let errors = err;
//         return res.status(500).json(errors);
//     }
// });

router.post('/default', (req, res, next) => {
    const newDefault = new default_time({
        default_time: 0
    });
    newDefault
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Default Time Added Successfully...!',
            });
        })
        .catch(err => {
            res.status(500).json({ error: err });
        });
});

//Update Default Wait Time 
router.patch('/default_wait_time', (req, res, next) => {
    const body = req.body;
    const default_wait_time = 0;
    default_time.findOne({ _id: body.id })
        .exec()
        .then(Default_time => {
            if (body.default_time == null) {
                default_time.findByIdAndUpdate({ _id: body.id }, { $set: { default_time: default_wait_time } })
                    .exec()
                    .then(result => {
                        res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
                    })
                    .catch(err => {
                        res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
                    });
            }
            else {
                default_time.findByIdAndUpdate({ _id: body.id }, { $set: { default_time: req.body.default_time } })
                    .exec()
                    .then(result => {
                        res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
                    })
                    .catch(err => {
                        res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
                    });

            }
        });
});
module.exports = router;