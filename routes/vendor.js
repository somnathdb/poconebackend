const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Vendor = require('../models/vendor');
const Dinner = require('../models/add_dinner');
const keys = require('../config/keys');


const app = express();
app.use(bodyParser.json());

//Login validation
const validateLoginInput = require('../validation/login');
// Load register validation 
const validateRegisterInput = require('../validation/register');
//Update Vendor
const updateVendor = require('../validation/update');
//update password
const passwordUpdate = require('../validation/password');


// Get the All vendor List Using /all_vendor_list route
router.get('/all_vendor_list', async (req, res, next) => {
    const role = 'vendor'
    Vendor.find({ role: role })
        .exec()
        .then(async vendorList => {
            let returnList = [];
            for (var i = 0; i < vendorList.length; i++) {
                let editableVendorList = {};
                var eachVendorId = vendorList[i].id;
                let dinnerListForVendor = await Dinner.find({ vendor_id: eachVendorId });
                //console.log( dinnerListForVendor );
                let totalSeated = 0, totalNonSeated = 0;
                for (var j = 0; j < dinnerListForVendor.length; j++) {
                    if (dinnerListForVendor[j].status === 'seated') {
                        totalSeated = totalSeated + 1;
                    }
                    if (dinnerListForVendor[j].status === 'Notseated') {
                        totalNonSeated = totalNonSeated + 1;
                    }
                }
                //console.log( totalSeated,totalNonSeated  );
                // PUSH TOTAL SEATED AND NON SEATED STATE
                editableVendorList.role = vendorList[i].role;
                editableVendorList._id = vendorList[i]._id;
                editableVendorList.vendor_name = vendorList[i].vendor_name,
                    editableVendorList.email = vendorList[i].email,
                    editableVendorList.phone_number = vendorList[i].phone_number,
                    editableVendorList.address = vendorList[i].address,
                    editableVendorList.username = vendorList[i].username,
                    editableVendorList.totalSeated = totalSeated;
                editableVendorList.totalNonSeated = totalNonSeated;
                //console.log( editableVendorList );
                returnList.push(editableVendorList);
            }
            res.status(200).json(returnList);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
});

// Add the vendor using /signup route (Here Username is unique)

router.post('/signup', (req, res, next) => {
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Vendor.findOne({ username: req.body.username })
        .then(vendor => {
            if (vendor) {
                return res.status(400).json({ username: 'Username already Exists' });
            }
            else {
                const newVendor = new Vendor({
                    vendor_name: req.body.vendor_name,
                    email: req.body.email,
                    phone_number: req.body.phone_number,
                    address: req.body.address,
                    username: req.body.username,
                    password: req.body.password,
                    role: "vendor"
                });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newVendor.password, salt, (err, hash) => {
                        if (err) throw err;
                        newVendor.password = hash;
                        newVendor.save()
                            .then(vendor => res.json("Vendor Created Successfully"))
                            .catch(err => console.log(err));
                    })
                })
            }
        })

});

// Login the Vendor Using /login route (Enter Username and password)

router.post('/login', (req, res, next) => {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const username = req.body.username;
    const password = req.body.password;
    Vendor.findOne({ username }).then(vendor => {
        if (!vendor) {
            return res.status(404).json({ username: 'username not found' });
        }
        bcrypt.compare(password, vendor.password).then(isMatch => {
            if (isMatch) {
                const payload = { id: vendor.id, role: vendor.role, username: vendor.username, password: vendor.password, vendor_name: vendor.vendor_name }
                jwt.sign(payload, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
                    res.json({
                        success: true,
                        token: 'Bearer ' + token
                    })
                });
            }
            else {
                return res.status(400).json({ password: 'password Incurrect' });
            }
        });
    });
}
);


// Update the Informtion using VendorId route by PATCH method

router.patch('/:vendorId', (req, res, next) => {
    const { errors, isValid } = updateVendor(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const id = req.params.vendorId;
    console.log(req.body);
    Vendor.findByIdAndUpdate({ _id: id }, {
        $set: {
            vendor_name: req.body.vendor_name, email: req.body.email,
            phone_number: req.body.phone_number, address: req.body.address, username: req.body.username
        }
    })
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({ message: 'Update  SuccessFully..!' });
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ message: 'Update Not SuccessFully..!' });
        });

});

// Update password using vendor id

router.patch('/password/:vendorId', (req, res, next) => {
    const { errors, isValid } = passwordUpdate(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const id = req.params.vendorId;
    console.log(req.body);
    Vendor.findByIdAndUpdate({ _id: id }, { $set: { password: req.body.password, password2: req.body.password } })
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({ message: 'Password Update SuccessFully..!' });
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ message: 'Password Not Update SuccessFully..!' });
        });

});


router.get('/check_payload/:username', (req, res, next) => {
    const username = req.body.username;
    Vendor.findOne({ username }).then(vendor => {
        if (!vendor) {
            return res.status(404).json({ username: 'username not found' });
        }
        const payload = { id: vendor.id, username: vendor.username }
        res.json({
            success: true,
            payload: 'pay' + payload
        })
    });
});

// Get All dinner list (show all)
router.get('/dinner_list/get_all/:vendor_id', (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    Dinner.find({ vendor_id: vendor_id })
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
});

//Get Dinner list (Dinner List Of perticular Vendor)
router.get('/dinner_list/:vendor_id', (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'Notseated'
    Dinner.find({ vendor_id: vendor_id, status: status })
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
});

//Get Dinner List (Seated)
router.get('/dinner_list/seated/:vendor_id', (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'seated'
    Dinner.find({ vendor_id: vendor_id, status: status })
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
});

//Get single Vendor information by using /:username route(BY username)

router.get('/:username', (req, res, next) => {
    const username = req.params.username;
    Vendor.find({ username: username })
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
});







    // Search Handler

    router.get('/searchHandler', (req, res, next) => {
        const body = req.body;
        const searchvalue = body.value;
        console.log(searchvalue)
        var search = new Regex(searchvalue, 'i');
        let searchResult = [];
        let allDinner = Dinner.find();
        for (var i = 0; i < allDinner.length; i++) {
            if (search.test(allDinner[i].name) || search.test(allDinner[i].phone_number)) {
                searchResult.push(allDinner[i])
            }
        }
    });

    module.exports = router;


