const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Vendor = require('../models/vendor');
const Dinner = require('../models/add_dinner');
const keys = require('../config/keys');
var Nexmo = require('nexmo');

//Login validation
const validateLoginInput = require('../validation/login');
// Load register validation 
const validateRegisterInput = require('../validation/register');
//Update Vendor
const updateVendor = require('../validation/update');
//update password
const passwordUpdate = require('../validation/password');

//Get All Vendor List

exports.get_all_vendor = (req, res, next) => {
    Vendor.find()
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
}

// Add New Vendor

exports.vendor_signup = (req,res,next) => {
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Vendor.findOne({ username: req.body.username })
        .then(vendor => {
            if (vendor) {
                return res.status(400).json({ username: 'Username already Exists' });
            }
            else {
                const newVendor = new Vendor({
                    vendor_name: req.body.vendor_name,
                    email: req.body.email,
                    phone_number: req.body.phone_number,
                    address: req.body.address,
                    username: req.body.username,
                    password: req.body.password,
                    role: req.body.role
                });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newVendor.password, salt, (err, hash) => {
                        if (err) throw err;
                        newVendor.password = hash;
                        newVendor.save()
                            .then(vendor => res.json(vendor))
                            .catch(err => console.log(err));
                    })
                })
            }
        })

}

//Login Vendor
exports.vendor_login = (req, res,next) => {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const username = req.body.username;
    const password = req.body.password;
    Vendor.findOne({ username }).then(vendor => {
        if (!vendor) {
            return res.status(404).json({ username: 'username not found' });
        }
        bcrypt.compare(password, vendor.password).then(isMatch => {
            if (isMatch) {
                const payload = { id: vendor.id, role: vendor.role, username: vendor.username, password: vendor.password, vendor_name: vendor.vendor_name }
                console.log(payload);
                jwt.sign(payload, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
                    res.json({
                        success: true,
                        token: 'Bearer ' + token
                    })
                });
            }
            else {
                return res.status(400).json({ password: 'password Incurrect' });
            }
        });
    });
}

//Update Vendor using Vendor Id

exports.vendor_update = (req, res, next) => {
    const { errors, isValid } = updateVendor(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const id = req.params.vendorId;
    console.log(req.body);
    Vendor.findByIdAndUpdate({ _id: id }, { $set:{vendor_name:req.body.vendor_name ,email:req.body.email,
        phone_number:req.body.phone_number,address:req.body.address,username:req.body.username}})
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({ message: 'Update  SuccessFully..!' });
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ message: 'Update Not SuccessFully..!' });
        });

}

// Update password by vendorId

exports.vendor_password_update = (req, res, next) => {
    const { errors, isValid } = passwordUpdate(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const id = req.params.vendorId;
    console.log(req.body);
    Vendor.findByIdAndUpdate({ _id: id }, { $set:{password:req.body.password ,password2:req.body.password}})
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({ message: 'Password Update SuccessFully..!' });
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ message: 'Password Not Update SuccessFully..!' });
        });

}

// Get the Infomation using Username

exports.vendor_get_username = (req, res, next) => {
    const username = req.params.username;
    Vendor.find({ username: username })
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Check the Username using Payload

exports.vendor_check_payload = (req, res, next) => {
    const username = req.body.username;
    Vendor.findOne({ username }).then(vendor => {
        if (!vendor) {
            return res.status(404).json({ username: 'username not found' });
        }
        const payload = { id: vendor.id, username: vendor.username }
        console.log(payload);
        res.json({
            success: true,
            payload: 'pay' + payload
        })
    });
}
// Get all Dinner list using VendorId

exports.get_all_dinner_list = (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    Dinner.find({vendor_id: vendor_id })
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
}

//NotSeated Dinner List Of perticular Vendor
exports.get_Notseated_Dinner_list = (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'Notseated'
    Dinner.find({ vendor_id: vendor_id , status:status})
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Seated Dinner List Of perticular Vendor
exports.get_seated_Dinner_list = (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'seated'
    Dinner.find({ vendor_id: vendor_id , status:status})
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Get Dinner List Total (seated)
exports.get_total_seated = (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'seated'
    Dinner.find({ vendor_id: vendor_id, status: status })
        .exec()
        .then(doc => {
            var totalAdult = 0, totalChild = 0 ,total = 0;
            for (let val of doc) {
                const adults = val.adults;
                const kids = val.kids;
                totalAdult = totalAdult + adults;
                totalChild = totalChild + kids;
                total_seated = totalAdult + totalChild;
            }
            res.status(200).json({totalAdult,totalChild,total_seated});
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Get Dinner List Total (NotSeated)

exports.get_total_Notseated =  (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    const status = 'Notseated'
    Dinner.find({ vendor_id: vendor_id, status: status })
        .exec()
        .then(doc => {
            var totalAdult = 0, totalChild = 0 ,total = 0;
            for (let val of doc) {
                const adults = val.adults;
                const kids = val.kids;
                totalAdult = totalAdult + adults;
                totalChild = totalChild + kids;
                total_notseated = totalAdult + totalChild;
            }
            res.status(200).json({totalAdult,totalChild,total_notseated});
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Get Dinner List Total (Seated Or NotSeated)

exports.get_total_all =  (req, res, next) => {
    const vendor_id = req.params.vendor_id;
    Dinner.find({ vendor_id: vendor_id })
        .exec()
        .then(doc => {
            var totalAdult = 0, totalChild = 0;
            for (let val of doc) {
                const adults = val.adults;
                const kids = val.kids;
                totalAdult = totalAdult + adults;
                totalChild = totalChild + kids;
                total_people = totalAdult + totalChild;
            }
            res.status(200).json({totalAdult,totalChild,total_people});
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}


//send sms
exports.send_sms =  (req, res, next) => {
const nexmo = new Nexmo({
  apiKey: '062be021',
  apiSecret: 'CjLf0zOCIHKo1o9v',
});

const from = 'Nexmo';
const to = '917020760846';
const text = 'Hello from Nexmo';

nexmo.message.sendSms(from, to, text);
}