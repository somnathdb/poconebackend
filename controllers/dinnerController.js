const Dinner = require('../models/add_dinner');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');

//Get all Dinner List

exports.get_all_dinner_list = (req, res, next) => {
    Dinner.find()
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
}

// Add New Dinner
exports.add_dinner = (req, res, next) => {
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization;
        const token = authorization.split(" ")[1];
        try {
            decoded = jwt.verify(token, keys.secretOrKey);
        } catch (e) {
            return res.status(401).send('unauthorized');
        }
        const vendorId = decoded.id;
        const dinner = new Dinner({
            vendor_id: vendorId,
            name: req.body.name,
            phone_number: req.body.phone_number,
            adults: req.body.adults,
            kids: req.body.kids,
            total:parseInt(req.body.adults)+parseInt(req.body.kids),
            wait_time:req.body.wait_time,
            special_occassion: req.body.special_occassion,
            status: req.body.status
        });
        dinner
            .save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    message: 'Dinner Information Added Successfully...!',
                    createDinner: dinner
                });
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({ error: err });
            });
    }
}

//Get all dinner (Show Notseated Button)

exports.get_dinner_status_Notseated = (req, res, next) => {
    const id = req.params.vendorId;
    const status = 'Notseated';
    Dinner.find({ vendor_id: id , status: status})
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

//Get all dinner (Show All Button)

exports.get_dinner_status = (req, res, next) => {
    const id = req.params.vendorId;
    Dinner.find({ vendor_id: id })
        .exec()
        .then(doc => {
            console.log(doc)
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

