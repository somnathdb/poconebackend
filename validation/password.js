const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data){
  let errors = {};

  data.password = !isEmpty(data.password) ? data.password: '';
  data.password2 = !isEmpty(data.password2) ? data.password2: '';

//passord

    if(!Validator.isLength(data.password, { min:6, max:30 })){
    errors.password = 'password must be between 6 and 30 characters';
  }

  if(Validator.isEmpty(data.password)){
    errors.password = 'Password field is required';
  }

  //passowrd2

  if(!Validator.equals(data.password, data.password2)){
    errors.password2 = 'Password must match';
  }

  if(Validator.isEmpty(data.password2)){
    errors.password2 = 'Confirm password is required';
  }



  return{
    errors,
    isValid: isEmpty(errors)
  }
}