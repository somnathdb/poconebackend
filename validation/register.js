const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data){
  let errors = {};

  data.username = !isEmpty(data.username) ? data.username: ''; 
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number: '';
  data.email = !isEmpty(data.email) ? data.email: '';
  data.password = !isEmpty(data.password) ? data.password: '';
  data.password2 = !isEmpty(data.password2) ? data.password2: '';

  // First name

  if(!Validator.isLength(data.username, { min:2, max:30 })){
    errors.username = 'Username must be between 2 and 30 characters';
  }

  if(Validator.isEmpty(data.username)){
      errors.username = 'Username is required';
  }

 

  // Email

  if(!Validator.isEmail(data.email, { min:2, max:30 })){
    errors.email = 'Eamail is in valid';
  }

  if(Validator.isEmpty(data.email)){
    errors.email = 'email field is required';
  }


// Mobile no

  if(!Validator.isLength(data.phone_number, { min:10, max:10 })){
    errors.phone_number = 'Mobile no is in valid';
  }

  if(Validator.isEmpty(data.phone_number)){
    errors.phone_number = 'mobile no is required';
  }

//passord

  

  if(!Validator.isLength(data.password, { min:6, max:30 })){
    errors.password = 'password must be between 6 and 30 characters';
  }

  if(Validator.isEmpty(data.password)){
    errors.password = 'Password field is required';
  }

  //passowrd2

  if(!Validator.equals(data.password, data.password2)){
    errors.password2 = 'Password must match';
  }

  if(Validator.isEmpty(data.password2)){
    errors.password2 = 'Confirm password is required';
  }



  return{
    errors,
    isValid: isEmpty(errors)
  }
}