const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
    let errors = {};

    data.username = !isEmpty(data.username) ? data.username : '';
    data.mobileNo = !isEmpty(data.mobileNo) ? data.mobileNo : '';


    // Mobile no

    if (!Validator.isLength(data.mobileNo, { min: 10, max: 10 })) {
        errors.mobileNo = 'Mobile no is in valid';
    }

    if (Validator.isEmpty(data.mobileNo)) {
        errors.mobileNo = 'mobile no is required';
    }

    // Address

    if (!Validator.isLength(data.message, { min: 5, max: 30 })) {
        errors.message = 'message must be between 5 and 30 characters';
    }

    if (Validator.isEmpty(data.message)) {
        errors.message = 'message is required';
    }




    return {
        errors,
        isValid: isEmpty(errors)
    }
}