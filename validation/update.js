const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
    let errors = {};

    data.username = !isEmpty(data.username) ? data.username : '';
    data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.vendor_name = !isEmpty(data.vendor_name) ? data.vendor_name : '';
    data.address = !isEmpty(data.address) ? data.address : '';


    // First name

    if (!Validator.isLength(data.username, { min: 2, max: 30 })) {
        errors.username = 'Userame must be between 2 and 30 characters';
    }

    if (Validator.isEmpty(data.username)) {
        errors.username = 'Username is required';
    }



    // Email

    if (!Validator.isEmail(data.email, { min: 2, max: 30 })) {
        errors.email = 'Email is in valid';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'email field is required';
    }


    // Mobile no

    if (!Validator.isLength(data.phone_number, { min: 10, max: 10 })) {
        errors.phone_number = 'Mobile no is in valid';
    }

    if (Validator.isEmpty(data.phone_number)) {
        errors.phone_number = 'mobile no is required';
    }

    // Address

    if (!Validator.isLength(data.address, { min: 5, max: 30 })) {
        errors.address = 'Address must be between 5 and 30 characters';
    }

    if (Validator.isEmpty(data.address)) {
        errors.address = 'Address is required';
    }

    // Vendor name

    if (!Validator.isLength(data.vendor_name, { min: 2, max: 30 })) {
        errors.vendor_name = 'Vendor Name must be between 2 and 30 characters';
    }

    if (Validator.isEmpty(data.vendor_name)) {
        errors.vendor_name = 'Vendor Name is required';
    }



    return {
        errors,
        isValid: isEmpty(errors)
    }
}